package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Random;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {
	private ArrayList<User> user = new ArrayList<>();
	int i = 0;
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
	private static Random random = new Random();
	private static String[] firstNames = {"John", "Emma", "Olivia", "Ava", "Isabella", "Sophia", "Robin"};
	String randomName = firstNames[random.nextInt(firstNames.length)];
/*
	//	Retrieve all posts
	//localhost:8080/posts
	@RequestMapping(value="/posts", method = RequestMethod.GET)
	public String getPosts(){
		return "All posts retrieved.";
	}
	//Creating new post
	//localhost:8080/posts
	@RequestMapping(value ="/posts", method = RequestMethod.POST)
	public String createPosts(){
		return "New post created.";
	}

	//Retrieving a single post
	//localhost:8080/posts/1234
	@RequestMapping(value="/posts/{postid}", method = RequestMethod.GET)
	public String getPosts(@PathVariable Long postid){
		return "Viewing details of post " + postid;
	}
	//Deleting a post
	//localhost:8080/post/1234
	@RequestMapping(value="/posts/{postid}", method = RequestMethod.DELETE)
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + "has been deleted.";
	}
	//Updating a post
	//localhost:8080/posts/1234
	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
	//Automatically coverts the format to JSON.
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	//Retrieving a post for a particular user.
	//localhost:8080/myPosts
	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	public String getMyPosts(@RequestHeader(value ="Authorization") String user){
		return "Posts for" + user + " have been retrieved";
	}*/
	//Assignment
//Create a user
@PostMapping("/users")
public String createUser(){
	i++;
	User newuser = new User(i+"",firstNames[random.nextInt(firstNames.length)]);
	user.add(newuser);
	return "new user created";
}
	//Get all the user
	@GetMapping("/users")
	public String getallUser(){
		return "All users retrieved";
	}
	// Get a specific user
	@GetMapping("/users/{userID}")
	public String getUser(@PathVariable String userID){
		String message = "ID: "+userID+" was not found";
		for(User users: user){
			if(users.getId().equals(userID)){
				message = (users.getName()+"'s data was retrieved with the ID :"+ users.getId());
				break;
			}
		}
		return message;
	}
	//Delete a user
	@DeleteMapping("/users/{userID}")
	public String deleteUser(@PathVariable Long userID){
		return "The post " + userID + " has been deleted.";
	}

	//update user

	@PutMapping("/users/{userID}")
	public User updateUser(@PathVariable Long userID, @RequestBody User user){
		return user;
	}
}
